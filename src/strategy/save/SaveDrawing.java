package strategy.save;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.imageio.ImageIO;

import frame.DrawingFrame;

public class SaveDrawing implements SaveStrategy {
	
	@Override
	public void save(DrawingFrame drawingFrame, File file) {

		try {
		
			ObjectOutputStream save = new ObjectOutputStream(new FileOutputStream(file));
			save.writeObject(drawingFrame.getDrawingView().getDrawingmodel().getShapes());
			
			save.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
