package strategy.save;

import java.io.File;

import frame.DrawingFrame;

public class SaveManager implements SaveStrategy {
	
	private SaveStrategy saveStrategy;
	
	public SaveManager(SaveStrategy saveStrategy) {
		this.saveStrategy = saveStrategy;
	}

	@Override
	public void save(DrawingFrame drawingFrame, File file) {
		saveStrategy.save(drawingFrame, file);
		
	}

}
