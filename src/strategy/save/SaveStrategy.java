package strategy.save;

import java.io.File;

import frame.DrawingFrame;

public interface SaveStrategy {
	
	public void save(DrawingFrame drawingFrame, File file);

}
