package strategy.open;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import frame.DrawingFrame;
import mvc.model.DrawingModel;

public class OpenLog implements OpenStrategy {

	@Override
	public void open(DrawingModel drawingModel, DrawingFrame drawingFrame, File file)  {
		String line = null;
		 try {
	    
	            FileReader fileReader = new FileReader(file);

	            BufferedReader bufferedReader = new BufferedReader(fileReader);
	            drawingFrame.getButtonController().makeNewDrawing();

	            
	            while((line = bufferedReader.readLine()) != null) {

	            	drawingModel.getLogList().add(line);
	            }   

	            bufferedReader.close();   
	            fileReader.close();
	        }
	        catch(FileNotFoundException ex) {
	            System.out.println("Unable to open file '" + file + "'");                
	        }
	        catch(IOException ex) {
	            System.out.println("Error reading file '" + file + "'");                  
	            ex.printStackTrace();
	        }
		
	}

}
