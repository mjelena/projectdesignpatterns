package strategy.open;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import frame.DrawingFrame;
import mvc.model.DrawingModel;
import shape.Shape;

public class OpenDrawing  implements OpenStrategy {

	@Override
	public void open(DrawingModel drawingModel, DrawingFrame drawingFrame, File file) {
		
		
		try {
			FileInputStream in=new FileInputStream(file);
			ObjectInputStream o=new ObjectInputStream(in);
			
		    drawingFrame.getButtonController().makeNewDrawing();
			drawingModel.getShapes().addAll((ArrayList<Shape>)o.readObject());
		
			
			
			o.close();
			in.close();
		        
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
	

}
