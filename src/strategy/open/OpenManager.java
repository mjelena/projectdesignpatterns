package strategy.open;

import java.io.File;

import frame.DrawingFrame;
import mvc.model.DrawingModel;

public class OpenManager implements OpenStrategy {
	private OpenStrategy openStrategy;
	
	public OpenManager(OpenStrategy openStrategy) {
		this.openStrategy = openStrategy;
	}

	@Override
	public void open(DrawingModel drawingModel,DrawingFrame drawingFrame, File file) {
		openStrategy.open(drawingModel, drawingFrame, file);
		
	}

}
