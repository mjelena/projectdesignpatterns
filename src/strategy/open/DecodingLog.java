package strategy.open;

import java.awt.Color;

import frame.DrawingFrame;
import hexagon.Hexagon;
import mvc.model.DrawingModel;
import shape.Shape;
import shape.circle.Circle;
import shape.circle.UpdateCircle;
import shape.hexagon.HexagonAdapter;
import shape.hexagon.UpdateHexagon;
import shape.line.Line;
import shape.line.UpdateLine;
import shape.point.Point;
import shape.point.UpdatePoint;
import shape.rectangle.Rectangle;
import shape.rectangle.UpdateRectangle;
import shape.square.Square;
import shape.square.UpdateSquare;

public class DecodingLog {
	
	public void lineLogDecoding(DrawingFrame drawingFrame, DrawingModel drawingModel, int tempLog ) {
		String fileLine = drawingModel.getLogList().get(tempLog);
		String[] parts = fileLine.split(";");

		int x = Integer.parseInt(parts[0].substring(parts[0].indexOf("(")+1, parts[0].indexOf(",")));
		int y = Integer.parseInt(parts[0].substring(parts[0].indexOf(",")+1, parts[0].indexOf(")")));
		
		if(fileLine.contains("Point")) {
		
			String[] color = parts[1].split(":");
			
			Point point  = new Point(x,y, Color.decode(color[1]));
			if(fileLine.contains("Add")) {
				drawingFrame.getDrawingController().addPointOnView(point);
			} else if (fileLine.contains("Update")) {
				UpdatePoint updatePointCommand = new UpdatePoint((Point)drawingModel.getSelectedShapes().get(0), point,
						 drawingFrame.getLogView());
				updatePointCommand.execute();
				drawingModel.getUndoStack().offerLast(updatePointCommand);
			} else {
				checkFileLineCommand(point, drawingFrame, fileLine, parts);
			}
		} else if (fileLine.contains("Line")) {
		
			int endX = Integer.parseInt(parts[1].substring(parts[1].indexOf("(")+1, parts[1].indexOf(",")));
			int endY = Integer.parseInt(parts[1].substring(parts[1].indexOf(",")+1, parts[1].indexOf(")")));
			String[] color = parts[2].split(":");
			
			Line line = new Line(new Point(x, y), new Point(endX, endY), Color.decode(color[1]));
			if(fileLine.contains("Add")) {
				drawingFrame.getDrawingController().addLineOnView(line);
			} else if (fileLine.contains("Update")) {
				UpdateLine updateLineCommand = new UpdateLine((Line)drawingModel.getSelectedShapes().get(0), line,
						 drawingFrame.getLogView());
				updateLineCommand.execute();
				drawingModel.getUndoStack().offerLast(updateLineCommand);
			} else {
				checkFileLineCommand(line, drawingFrame, fileLine, parts);
			}
			
		} else if (fileLine.contains("Circle")) {
			
			
			String[] radius = parts[1].split(":");
			String[] outerColor = parts[2].split(":");
			String[] insideColor = parts[3].split(":");
			
			
			
		
		
			Circle circle = new Circle(new Point(x,y), Integer.parseInt(radius[1]), Color.decode(outerColor[1]), Color.decode(insideColor[1]));
			if(fileLine.contains("Add")) {
				drawingFrame.getDrawingController().addCircleOnView(circle);
			}  else if (fileLine.contains("Update")) {
				System.out.println(drawingModel.getSelectedShapes().get(0));
				
				UpdateCircle updateCircleCommand = new UpdateCircle((Circle)drawingModel.getSelectedShapes().get(0), circle,
						 drawingFrame.getLogView());
				updateCircleCommand.execute();
				drawingModel.getUndoStack().offerLast(updateCircleCommand);
				
			} else {
				checkFileLineCommand(circle, drawingFrame, fileLine, parts);
			}
			
		} else if (fileLine.contains("Square")) {
		
			
			String[] lenght = parts[1].split(":");
			String[] outerColor = parts[2].split(":");
			String[] insideColor = parts[3].split(":");
		
			Square square = new Square(new Point(x,y), Integer.parseInt(lenght[1]), Color.decode(outerColor[1]), Color.decode(insideColor[1]));
			if(fileLine.contains("Add")) {
				drawingFrame.getDrawingController().addSquareOnView(square);
			}  else if (fileLine.contains("Update")) {
				UpdateSquare updateSquareCommand = new UpdateSquare((Square)drawingModel.getSelectedShapes().get(0), square,
						 drawingFrame.getLogView());
				updateSquareCommand.execute();
				drawingModel.getUndoStack().offerLast(updateSquareCommand);
			} else {
				checkFileLineCommand(square, drawingFrame, fileLine, parts);
			}
			
		} else if (fileLine.contains("Rectangle")) {
			
			
			String[] lenght = parts[1].split(":");
			String[] width = parts[2].split(":");
			String[] outerColor = parts[3].split(":");
			String[] insideColor = parts[4].split(":");
		
			Rectangle rectangle = new Rectangle(new Point(x,y), Integer.parseInt(lenght[1]), Integer.parseInt(width[1]), Color.decode(outerColor[1]), Color.decode(insideColor[1])); 
			if(fileLine.contains("Add")) {
				drawingFrame.getDrawingController().addRectangleOnView(rectangle);
			}  else if (fileLine.contains("Update")) {
				UpdateRectangle updateRectangleCommand = new UpdateRectangle((Rectangle)drawingModel.getSelectedShapes().get(0), rectangle,
						 drawingFrame.getLogView());
				updateRectangleCommand.execute();
				drawingModel.getUndoStack().offerLast(updateRectangleCommand);
			} else { checkFileLineCommand(rectangle, drawingFrame, fileLine, parts);
				
			}
			
		}  else if (fileLine.contains("Hexagon")) {

			
			String[] radius = parts[1].split(":");
			
			String[] outerColor = parts[2].split(":");
			String[] insideColor = parts[3].split(":");
		
	
			HexagonAdapter hexagon = new HexagonAdapter(new Hexagon(x,y, Integer.parseInt(radius[1])), Color.decode(outerColor[1]), Color.decode(insideColor[1]));
			if(fileLine.contains("Add")) {
				drawingFrame.getDrawingController().addHexagonOnView(hexagon);
			} else if (fileLine.contains("Update")) {
				UpdateHexagon updateHexagonCommand = new UpdateHexagon((HexagonAdapter)drawingModel.getSelectedShapes().get(0), hexagon,
						 drawingFrame.getLogView());
				updateHexagonCommand.execute();
				drawingModel.getUndoStack().offerLast(updateHexagonCommand);
			} else {
				checkFileLineCommand(hexagon, drawingFrame, fileLine, parts);
			}
		} 
		
		
		
		
	}
	
	public void checkFileLineCommand(Shape shape, DrawingFrame drawingFrame, String fileLine, String[] parts) {
		
		if (fileLine.contains("Undo")) {
			drawingFrame.getButtonController().undoClick();
			
		} else if (fileLine.contains("Delete")) {
			drawingFrame.getButtonController().checkDeleteSelectedShape(shape);
		}  else if (fileLine.contains("To back")) {
			drawingFrame.getButtonController().toBack();
			
		} else if (fileLine.contains("To front")) {
			drawingFrame.getButtonController().toFront();
			
		}  else if (fileLine.contains("Bring to back")) {
			drawingFrame.getButtonController().bringToBack();
			
		} else if (fileLine.contains("Bring to front")) {
			drawingFrame.getButtonController().bringToFront();
			
		} else if (fileLine.contains("Select")) {
			String[] selectedCoordinates;
			if (shape instanceof Rectangle) {
				selectedCoordinates =  parts[5].split(": ");
			} else if (shape instanceof Line){
				selectedCoordinates =  parts[3].split(": ");
			
			} else if (shape instanceof Point){
				selectedCoordinates =  parts[2].split(": ");
			}
			else {
				selectedCoordinates =  parts[4].split(": ");
			}
			int Sx = Integer.parseInt(selectedCoordinates[1].substring(selectedCoordinates[1].indexOf("(")+1, selectedCoordinates[1].indexOf(",")));
			int Sy = Integer.parseInt(selectedCoordinates[1].substring(selectedCoordinates[1].indexOf(",")+1, selectedCoordinates[1].indexOf(")")));
			drawingFrame.getButtonController().selectedClick(Sx, Sy);
		} else if (fileLine.contains("Unselect")) {
			drawingFrame.getButtonController().unselectArray();
		} 
	} 

}
