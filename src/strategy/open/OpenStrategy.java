package strategy.open;

import java.io.File;

import frame.DrawingFrame;
import mvc.model.DrawingModel;

public interface OpenStrategy {

	public void open(DrawingModel drawingModel, DrawingFrame drawingFrame, File file);
}
