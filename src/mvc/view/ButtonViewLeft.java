package mvc.view;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class ButtonViewLeft extends JPanel {
	
	private JButton btnImportLog;
	private JButton btnLogNext;
	private JButton btnImportAllLog;
	private JButton btnImportDrawing;
	private JButton btnNewDrawing;
	private JButton btnSaveLog;
	private JButton btnSaveDrawing;
	
	
	public ButtonViewLeft () {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		btnSaveLog = new JButton("Save log");
		GridBagConstraints gbc_btnSaveLog = new GridBagConstraints();
		gbc_btnSaveLog.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSaveLog.insets = new Insets(0, 0, 5, 0);
		gbc_btnSaveLog.gridx = 0;
		gbc_btnSaveLog.gridy = 1;
		add(btnSaveLog, gbc_btnSaveLog);
		
		btnSaveDrawing = new JButton("Save drawing");
		GridBagConstraints gbc_btnSaveDrawing = new GridBagConstraints();
		gbc_btnSaveDrawing.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSaveDrawing.insets = new Insets(0, 0, 5, 0);
		gbc_btnSaveDrawing.gridx = 0;
		gbc_btnSaveDrawing.gridy = 2;
		add(btnSaveDrawing, gbc_btnSaveDrawing);
		
		btnImportLog = new JButton("Import log ");
		GridBagConstraints gbc_btnImportLog = new GridBagConstraints();
		gbc_btnImportLog.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnImportLog.insets = new Insets(0, 0, 5, 0);
		gbc_btnImportLog.gridx = 0;
		gbc_btnImportLog.gridy = 4;
		add(btnImportLog, gbc_btnImportLog);
		
		btnLogNext = new JButton("Log - next line");
		GridBagConstraints gbc_btnLogNext = new GridBagConstraints();
		gbc_btnLogNext.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLogNext.insets = new Insets(0, 0, 5, 0);
		gbc_btnLogNext.gridx = 0;
		gbc_btnLogNext.gridy = 5;
		add(btnLogNext, gbc_btnLogNext);
		
		btnImportAllLog = new JButton("Import all log");
		GridBagConstraints gbc_btnImportAllLog = new GridBagConstraints();
		gbc_btnImportAllLog.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnImportAllLog.insets = new Insets(0, 0, 5, 0);
		gbc_btnImportAllLog.gridx = 0;
		gbc_btnImportAllLog.gridy = 6;
		add(btnImportAllLog, gbc_btnImportAllLog);
		
		btnImportDrawing = new JButton("Import drawing");
		GridBagConstraints gbc_btnImportDrawing = new GridBagConstraints();
		gbc_btnImportDrawing.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnImportDrawing.insets = new Insets(0, 0, 5, 0);
		gbc_btnImportDrawing.gridx = 0;
		gbc_btnImportDrawing.gridy = 7;
		add(btnImportDrawing, gbc_btnImportDrawing);
		
		btnNewDrawing = new JButton("New drawing");
		GridBagConstraints gbc_btnNewDrawing = new GridBagConstraints();
		gbc_btnNewDrawing.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewDrawing.gridx = 0;
		gbc_btnNewDrawing.gridy = 9;
		add(btnNewDrawing, gbc_btnNewDrawing);
		
	}

	public JButton getBtnImportLog() {
		return btnImportLog;
	}

	public void setBtnImportLog(JButton btnImportLog) {
		this.btnImportLog = btnImportLog;
	}

	public JButton getBtnLogNext() {
		return btnLogNext;
	}

	public void setBtnLogNext(JButton btnLogNext) {
		this.btnLogNext = btnLogNext;
	}

	public JButton getBtnImportAllLog() {
		return btnImportAllLog;
	}

	public void setBtnImportAllLog(JButton btnImportAllLog) {
		this.btnImportAllLog = btnImportAllLog;
	}

	public JButton getBtnImportDrawing() {
		return btnImportDrawing;
	}

	public void setBtnImportDrawing(JButton btnImportDrawing) {
		this.btnImportDrawing = btnImportDrawing;
	}

	public JButton getBtnNewDrawing() {
		return btnNewDrawing;
	}

	public void setBtnNewDrawing(JButton btnNewDrawing) {
		this.btnNewDrawing = btnNewDrawing;
	}

	public JButton getBtnSaveLog() {
		return btnSaveLog;
	}

	public JButton getBtnSaveDrawing() {
		return btnSaveDrawing;
	}

}
