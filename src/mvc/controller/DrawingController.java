package mvc.controller;

import java.awt.Color;
import java.awt.event.MouseEvent;

import frame.DrawingFrame;
import hexagon.Hexagon;
import mvc.model.DrawingModel;
import shape.Shape;
import shape.circle.AddCircle;
import shape.circle.Circle;
import shape.hexagon.AddHexagon;
import shape.hexagon.HexagonAdapter;
import shape.line.AddLine;
import shape.line.Line;
import shape.point.AddPoint;
import shape.point.Point;
import shape.rectangle.AddRectangle;
import shape.rectangle.Rectangle;
import shape.square.AddSquare;
import shape.square.Square;

public class DrawingController {

	private DrawingModel drawingModel;
	private DrawingFrame drawingFrame;

	private Point firstPoint;
	private Shape draggedShape;
	
	public DrawingController(DrawingModel drawingModel, DrawingFrame drawingFrame) {
		this.drawingModel=drawingModel;
		this.drawingFrame=drawingFrame;
	}
	
	public void addPointOnView(Point point) {
		AddPoint addPoint=new AddPoint(drawingModel, point , drawingFrame.getLogView());
		addPoint.execute();
		drawingModel.getUndoStack().offerLast(addPoint);
		
	}
	
	public void addLineOnView(Line line) {
		AddLine addLine=new AddLine(drawingModel, line, drawingFrame.getLogView());
		addLine.execute();
		drawingModel.getUndoStack().offerLast(addLine);
		System.out.println("Line added");
	}
	
	public void addSquareOnView(Square square) {
		AddSquare addSquare=new AddSquare(drawingModel, square, drawingFrame.getLogView());
		addSquare.execute();
		drawingModel.getUndoStack().offerLast(addSquare); 
		System.out.println("Square added");
	}
	
	public void addCircleOnView(Circle circle) {
		AddCircle addCircle=new AddCircle(drawingModel, circle, drawingFrame.getLogView());
		addCircle.execute();
		drawingModel.getUndoStack().offerLast(addCircle);
		System.out.println("Circe added");
		
	}
	
	public void addRectangleOnView(Rectangle rectangle) {
		AddRectangle addRectangle=new AddRectangle(drawingModel, rectangle, drawingFrame.getLogView());
		addRectangle.execute();
		drawingModel.getUndoStack().offerLast(addRectangle);
	}
	
	public void addHexagonOnView(HexagonAdapter hexagon) {
		AddHexagon addHexagon=new AddHexagon(drawingModel, hexagon, drawingFrame.getLogView());
		addHexagon.execute();
		drawingModel.getUndoStack().offerLast(addHexagon);
	}
	 
	public void drawShape(MouseEvent e) {
		
		if(draggedShape==null) {
			if(pickedShape().toLowerCase().equals("point")) {
				addPointOnView(new Point(e.getX(),e.getY(), pickedOutlineColor()));
				
			}
			return;
		}
		
		switch(pickedShape().toLowerCase()) {
		case "line": {
			addLineOnView((Line) draggedShape);
			drawingModel.removeShape(draggedShape);
			break;
		}
		case "square": {
			addSquareOnView((Square) draggedShape);
			drawingModel.removeShape(draggedShape);
			break;
		}
		case "circle": {
			addCircleOnView((Circle) draggedShape);
			drawingModel.removeShape(draggedShape);
			break;
		}
		case "rectangle": {
			addRectangleOnView((Rectangle) draggedShape);
			drawingModel.removeShape(draggedShape);
			break;
		}
		case "hexagon": {
			addHexagonOnView((HexagonAdapter) draggedShape);
			drawingModel.removeShape(draggedShape);
			break;
		}
		
		}
		
		
		drawingFrame.repaint();
		draggedShape=null;

	}
	
	public void handleMousePressed(MouseEvent e) {
		if(pickedShape().equals("Point")) return;
		
	
		firstPoint=new Point(e.getX(), e.getY());
		
		
	}
	
	public void handleMouseDragged(MouseEvent e) {
		if(firstPoint == null) return;
		if(draggedShape!=null) {
			drawingModel.removeShape(draggedShape);
			draggedShape=null;
		} 
	
		int distance=(int)Math.abs(firstPoint.distance(new Point(e.getX(),e.getY())));
		
		switch(pickedShape().toLowerCase()) {
		case "point": {
			return;
		}
		case "line": {
			draggedShape=new Line(firstPoint, new Point(e.getX(), e.getY(), pickedOutlineColor()), pickedOutlineColor());
			drawingModel.addShape(draggedShape);
			break;
		}
		case "square": {
			draggedShape=new Square(firstPoint,distance, pickedOutlineColor(), pickedInteriorColor());	
			drawingModel.addShape(draggedShape);
			break;
		}
		case "circle": {
			draggedShape=new Circle(firstPoint, distance, pickedOutlineColor(), pickedInteriorColor());
			drawingModel.addShape(draggedShape);
			break;
		}
		case "rectangle": {
			int XLength = (int) firstPoint.distance(new Point(e.getX(), firstPoint.getY()));
			int YWidth=(int) firstPoint.distance(new Point(firstPoint.getX(), e.getY()));
			draggedShape=new Rectangle(firstPoint, XLength,YWidth,pickedOutlineColor(), pickedInteriorColor() );
			drawingModel.addShape(draggedShape);
			break;
		}
		case "hexagon": {
			draggedShape=new HexagonAdapter(new Hexagon(firstPoint.getX(), firstPoint.getY(), distance), pickedOutlineColor(), pickedInteriorColor());
			drawingModel.addShape(draggedShape);
			break;
		}
		default: return ;
		}
	
		drawingFrame.repaint();
	}
	
	public String pickedShape() {
		return drawingFrame.getButtonView().getComboBox_Shapes().getSelectedItem().toString();
	}
	
	public Color pickedOutlineColor() {
		return drawingFrame.getButtonController().getOutlineColor();
	}
	
	public Color pickedInteriorColor() {
		return drawingFrame.getButtonController().getInteriorColor();
	}
	
	
	

}
