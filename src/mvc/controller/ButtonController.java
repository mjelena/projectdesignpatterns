package mvc.controller;

import java.awt.Color;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import dialogs.UpdateCircleDialog;
import dialogs.UpdateHexagonDialog;
import dialogs.UpdateLineDialog;
import dialogs.UpdatePointDialog;
import dialogs.UpdateRectangleDialog;
import dialogs.UpdateSquareDialog;
import frame.DrawingFrame;
import mvc.model.DrawingModel;
import shape.Command;
import shape.Shape;
import shape.circle.Circle;
import shape.circle.RemoveCircle;
import shape.circle.UpdateCircle;
import shape.hexagon.HexagonAdapter;
import shape.hexagon.RemoveHexagon;
import shape.hexagon.UpdateHexagon;
import shape.line.Line;
import shape.line.RemoveLine;
import shape.line.UpdateLine;
import shape.point.Point;
import shape.point.RemovePoint;
import shape.point.UpdatePoint;
import shape.rectangle.Rectangle;
import shape.rectangle.RemoveRectangle;
import shape.rectangle.UpdateRectangle;
import shape.square.RemoveSquare;
import shape.square.Square;
import shape.square.UpdateSquare;
import strategy.open.DecodingLog;
import strategy.open.OpenDrawing;
import strategy.open.OpenLog;
import strategy.open.OpenManager;
import strategy.save.SaveDrawing;
import strategy.save.SaveLog;
import strategy.save.SaveManager;
import zcommand.BringToBack;
import zcommand.BringToFront;
import zcommand.ToBack;
import zcommand.ToFront;

public class ButtonController {

	private DrawingFrame drawingFrame;
	private DrawingModel drawingModel;
	private SaveManager saveManager;
	private OpenManager openManager;
	private Color outlineColor=Color.BLACK;
	private Color interiorColor=Color.WHITE;
	private int tempLog;
	private DecodingLog decodingLog = new DecodingLog();


	public ButtonController(DrawingFrame drawingFrame, DrawingModel drawingModel) {
		super();
		this.drawingFrame = drawingFrame;
		this.drawingModel = drawingModel;
	}

	public void undoClick() {
		System.out.println(drawingModel.getUndoStack().size());
		System.out.println(drawingModel.getUndoStack());
		
		if (!drawingModel.getUndoStack().isEmpty()) {
			Command previousCommand = drawingModel.getUndoStack().pollLast();
			previousCommand.unexecute();
			drawingModel.getRedoStack().offerLast(previousCommand);
			drawingFrame.repaint();
		}
		drawingModel.notifyAllObservers();
	}

	public void redoClick() {
		if (!drawingModel.getRedoStack().isEmpty()) {
			System.out.println(drawingModel.getRedoStack().size());
			Command previousCommand = drawingModel.getRedoStack().pollLast();
			drawingModel.getUndoStack().offerLast(previousCommand);
			previousCommand.execute();
			drawingFrame.repaint();
		}
		drawingModel.notifyAllObservers();
	}

	public boolean selectedClick(int x, int y) {
		for (int i = drawingModel.getShapes().size() - 1; i >= 0; i--) {
			if (drawingModel.getShapes().get(i).contains(x, y)) {
				drawingModel.getShapes().get(i).setSelected(true);
				drawingModel.getSelectedShapes().add(drawingModel.getShapes().get(i));
				drawingFrame.getLogView().getModel().addElement("Select: " + drawingModel.getShape(i).toString() + "; click: (" + x +"," + y +")" );
				drawingModel.notifyAllObservers();
				zHelper();
				return true;
			}
		}
		drawingModel.countSelectedShapes();
		unselectArray();
		return false;
	}

	public void unselectArray() {
		for (int i = drawingModel.getSelectedShapes().size() - 1; i >= 0; i--) {
			drawingModel.getSelectedShapes().get(i).setSelected(false);
			drawingFrame.getLogView().getModel().addElement("Unselect: " + drawingModel.getSelectedShapes().get(i).toString());
		}
		
		drawingModel.notifyAllObservers();
		

	}

	public void deleteShape() {
		drawingModel.countSelectedShapes();
		if (!drawingModel.getShapes().isEmpty()) {
			if (!drawingModel.getSelectedShapes().isEmpty()) {
				Object[] answer = new Object[] { "Yes", "No" };
				int result = JOptionPane.showOptionDialog(null,
						"Are you sure you want to delete " + drawingModel.getSelectedShapes().size() + " shapes?", "Delete",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, answer, answer[0]);
				if (result == JOptionPane.YES_OPTION) {
					for (int i = drawingModel.getSelectedShapes().size() - 1; i >= 0; i--) {
						Shape shape = drawingModel.getSelectedShapes().get(i);

						if (checkDeleteSelectedShape(shape)) {

							drawingModel.getSelectedShapes().remove(drawingModel.getSelectedShapes().get(i));
						}
					}

				}
			} else {
				JOptionPane.showMessageDialog(null, "You need to select shape!", "Error!", JOptionPane.ERROR_MESSAGE);
			}

		} else {
			JOptionPane.showMessageDialog(null, "Array of shapes is empty!", "Error!!", JOptionPane.ERROR_MESSAGE);
		}
		drawingModel.notifyAllObservers();
	}
	
	public boolean checkDeleteSelectedShape(Shape shape) {
		
		if (shape instanceof Point) {

			RemovePoint removePoint = new RemovePoint(drawingModel, (Point) shape, drawingFrame.getLogView());
			removePoint.execute();
			drawingModel.getUndoStack().offerLast(removePoint);
		}

		else if (shape instanceof Line) {

			RemoveLine removeLine = new RemoveLine(drawingModel, (Line) shape, drawingFrame.getLogView());
			removeLine.execute();
			drawingModel.getUndoStack().offerLast(removeLine);
		} else if (shape instanceof Square) {

			RemoveSquare removeSquare = new RemoveSquare(drawingModel, (Square) shape, drawingFrame.getLogView());
			removeSquare.execute();
			drawingModel.getUndoStack().offerLast(removeSquare);
		} else if (shape instanceof Rectangle) {

			RemoveRectangle removeRectangle = new RemoveRectangle(drawingModel, (Rectangle) shape, drawingFrame.getLogView());
			removeRectangle.execute();
			drawingModel.getUndoStack().offerLast(removeRectangle);
		} else if (shape instanceof Circle) {

			RemoveCircle removeCircle = new RemoveCircle(drawingModel, (Circle) shape, drawingFrame.getLogView());
			removeCircle.execute();
			drawingModel.getUndoStack().offerLast(removeCircle);
		}
		else if (shape instanceof HexagonAdapter) {

			RemoveHexagon removeHexagon = new RemoveHexagon(drawingModel, (HexagonAdapter) shape, drawingFrame.getLogView());
			removeHexagon.execute();
			drawingModel.getUndoStack().offerLast(removeHexagon);
		}else {
			return false;
		}

		return true;
	}

	public void updateShape() {
		drawingModel.countSelectedShapes();
		if (!drawingModel.getSelectedShapes().isEmpty()) {
			if (drawingModel.getSelectedShapes().size() == 1) {

				checkUpdateSelectedShape(drawingModel.getSelectedShapes().get(0));
				
			} else {
				JOptionPane.showMessageDialog(null, "More than one shape are selected!", "Error!!",
						JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(null, "You need to select shape!", "Error!!", JOptionPane.ERROR_MESSAGE);
		}

	}
	
	public void checkUpdateSelectedShape(Shape shape) {
		if (shape instanceof Point) {
			UpdatePointDialog updatePoint = new UpdatePointDialog((Point) shape);
			if (updatePoint.getPoint() != null) {
				System.out.println(updatePoint.getPoint().toString());
				UpdatePoint updatePointCommand = new UpdatePoint((Point) shape,
						updatePoint.getPoint(), drawingFrame.getLogView());
				updatePointCommand.execute();
				drawingModel.getUndoStack().offerLast(updatePointCommand);
			}
			return;
		} 
		if (shape instanceof Line) {
			UpdateLineDialog updateLine = new UpdateLineDialog((Line) shape);
			if (updateLine.getLine() != null) {
				System.out.println(updateLine.getLine().toString());
				UpdateLine updateLineCommand = new UpdateLine((Line) shape,
						updateLine.getLine(), drawingFrame.getLogView());
				updateLineCommand.execute();
				drawingModel.getUndoStack().offerLast(updateLineCommand);
			}
			return;
			
		}
		if (shape instanceof Circle) {
			UpdateCircleDialog updateCircle = new UpdateCircleDialog((Circle) shape);
			if (updateCircle.getCircle() != null) {
				System.out.println(updateCircle.getCircle().toString());
				UpdateCircle updateCircleCommand = new UpdateCircle((Circle) shape,
						updateCircle.getCircle(), drawingFrame.getLogView());
				updateCircleCommand.execute();
				drawingModel.getUndoStack().offerLast(updateCircleCommand);
			}
			return;
			
		}
		if (shape instanceof Rectangle) {
			UpdateRectangleDialog updateRectangle = new UpdateRectangleDialog((Rectangle) shape);
			if (updateRectangle.getRectangle() != null) {
				System.out.println(updateRectangle.getRectangle().toString());
				UpdateRectangle updateRectangleCommand = new UpdateRectangle((Rectangle) shape,
						updateRectangle.getRectangle(), drawingFrame.getLogView());
				updateRectangleCommand.execute();
				drawingModel.getUndoStack().offerLast(updateRectangleCommand);
			}
			return;
			
		}
		if (shape instanceof Square) {
			UpdateSquareDialog updateSquare = new UpdateSquareDialog((Square) shape);
			if (updateSquare.getSquare() != null) {
				System.out.println(updateSquare.getSquare().toString());
				UpdateSquare updateSquareCommand = new UpdateSquare((Square) shape,
						updateSquare.getSquare(), drawingFrame.getLogView());
				updateSquareCommand.execute();
				drawingModel.getUndoStack().offerLast(updateSquareCommand);
			}
			return;
			
		}
		
		if (shape instanceof HexagonAdapter) {
			UpdateHexagonDialog updateHexagon = new UpdateHexagonDialog((HexagonAdapter) shape);
			if (updateHexagon.getHexagon() != null) {
				System.out.println(updateHexagon.getHexagon().toString());
				UpdateHexagon updateHexagonCommand = new UpdateHexagon((HexagonAdapter) shape,
						updateHexagon.getHexagon(), drawingFrame.getLogView());
				updateHexagonCommand.execute();
				drawingModel.getUndoStack().offerLast(updateHexagonCommand);
			}
			return;	
		}
		
	}

	
	
	public void changeOutlineColor() {
		Color newColor = JColorChooser.showDialog(null, "Choose color: ", outlineColor);
               if(newColor!=null) {
            		if(newColor==Color.BLACK) {
                		drawingFrame.getButtonView().getBtnOutlineColor().setForeground(Color.WHITE);
                	} else {
                		drawingFrame.getButtonView().getBtnOutlineColor().setForeground(Color.BLACK);
                	}
            	   drawingFrame.getButtonView().getBtnOutlineColor().setBackground(newColor);

            	   outlineColor=newColor;
               }
	}
	
	public void changeInteriorColor() {
		Color newColor = JColorChooser.showDialog(null, "Choose color: ", interiorColor);
        if(newColor!=null) {
        	if(newColor==Color.BLACK) {
        		drawingFrame.getButtonView().getBtnInteriorColor().setForeground(Color.WHITE);
        	} else {
        		drawingFrame.getButtonView().getBtnInteriorColor().setForeground(Color.BLACK);
        	}
     	   drawingFrame.getButtonView().getBtnInteriorColor().setBackground(newColor);
     	 
     	   interiorColor=newColor;
        }
        
	}
	
	public void saveLog() {
		
		if(drawingFrame.getLogView().getModel().isEmpty()) {
			System.out.println("Nemaa sta da see sacuva");
			return;
		}

		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("txt", "text");
		chooser.setFileFilter(filter);
		int answer = chooser.showSaveDialog(drawingFrame.getButtonViewLeft().getBtnSaveLog());
		File file = chooser.getSelectedFile();
		if(file.exists()) {
			JOptionPane.showMessageDialog(null, "A file with the name you specified already exists!");
		}
		
			 if (answer == JFileChooser.APPROVE_OPTION) {

					saveManager = new SaveManager(new SaveLog());
					saveManager.save(drawingFrame, file);
				
					
				} 
	}
	
	public void saveDrawing() {
		
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("ser","jpg", "jpeg", "png");
		chooser.setFileFilter(filter);
		int answer = chooser.showSaveDialog(drawingFrame.getButtonViewLeft().getBtnSaveDrawing());

		if (answer == JFileChooser.APPROVE_OPTION) {

			File file = chooser.getSelectedFile();
			saveManager = new SaveManager(new SaveDrawing());
			saveManager.save(drawingFrame, file);
		}
		
	}
	
	public void importLog() {
		
		JFileChooser chooser = new JFileChooser();
		int answer = chooser.showOpenDialog(null);
		if (answer == JFileChooser.OPEN_DIALOG) {
			
			if(!drawingModel.getLogList().isEmpty()) {
				drawingModel.getLogList().clear();
				this.tempLog = 0;
			}

			File file = chooser.getSelectedFile();
			openManager = new OpenManager(new OpenLog());
			openManager.open(drawingModel, drawingFrame, file);
		
			
		}	
	}
	
	public void logNextLine() {
		
		if(tempLog < drawingModel.getLogList().size()) {
			
			decodingLog.lineLogDecoding(drawingFrame, drawingModel, tempLog);
			tempLog++;
		}
		
	}
	
	public void importAllLog() {
		
		for(int i=0; i<drawingModel.getLogList().size(); i++) {
			decodingLog.lineLogDecoding(drawingFrame, drawingModel, i);
		}
		
	}
	
	public void bringToFront() {
		
		BringToFront commandBringToFront = new BringToFront(drawingModel, drawingFrame.getLogView());
		commandBringToFront.execute();
		drawingModel.getUndoStack().offerLast(commandBringToFront);
		drawingFrame.repaint();
		zHelper();
		
		
	}
	
	public void bringToBack() {
		
			
			BringToBack commandBringToBack = new BringToBack(drawingModel, drawingFrame.getLogView());
			commandBringToBack.execute();
			drawingModel.getUndoStack().offerLast(commandBringToBack);
			drawingFrame.repaint();
			zHelper();
		
		
	}
	
	public void toFront() {
		
		ToFront commandToFront = new ToFront(drawingModel, drawingFrame.getLogView());
		commandToFront.execute();
		drawingModel.getUndoStack().offerLast(commandToFront);
		drawingFrame.repaint();
		zHelper();
		
	}
	
	public void toBack() {
		

		
		ToBack commandToBack = new ToBack(drawingModel, drawingFrame.getLogView(), drawingFrame);
		commandToBack.execute();
		drawingModel.getUndoStack().offerLast(commandToBack);
		drawingFrame.repaint();
		zHelper();
		
		
	}
	
	public void zHelper() {
		if(drawingModel.countSelectedShapes() == 1) {
	
		if(drawingModel.getShapes().get(drawingModel.getShapes().size()-1) == drawingModel.getSelectedShapes().get(0)) {
			drawingFrame.getButtonViewRight().getBtnBringToFront().setEnabled(false);
			drawingFrame.getButtonViewRight().getBtnToFront().setEnabled(false);
		} else {
			drawingFrame.getButtonViewRight().getBtnBringToFront().setEnabled(true);
			drawingFrame.getButtonViewRight().getBtnToFront().setEnabled(true);
		}
		if (drawingModel.getShapes().get(0) == drawingModel.getSelectedShapes().get(0)) {
			drawingFrame.getButtonViewRight().getBtnBringToBack().setEnabled(false);
			drawingFrame.getButtonViewRight().getBtnToBack().setEnabled(false);
		} else {
			drawingFrame.getButtonViewRight().getBtnBringToBack().setEnabled(true);
			drawingFrame.getButtonViewRight().getBtnToBack().setEnabled(true);
		}
		}
		else {
			drawingModel.notifyAllObservers();
		}
	}
	
	public void importDrawing() {
		JFileChooser chooser = new JFileChooser();
		int answer = chooser.showOpenDialog(null);
		if (answer == JFileChooser.OPEN_DIALOG) {
			
			if(!drawingModel.getLogList().isEmpty()) {
				drawingModel.getLogList().clear();
				this.tempLog = 0;
			}

			File file = chooser.getSelectedFile();
		
			openManager = new OpenManager(new OpenDrawing());
			openManager.open(drawingModel, drawingFrame, file);
			drawingFrame.repaint();
		
			
		} 
		
	}
	
	public void newDrawing() {
		Object[] answer = new Object[] { "Yes", "No" };
		int result = JOptionPane.showOptionDialog(null,
				"Are you sure you want new drawing? ", "New drawing",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, answer, answer[0]);
		
		if (result == JOptionPane.YES_OPTION) {
			makeNewDrawing();
		}
		
		
	
	}
	
	public void makeNewDrawing() {
		drawingModel.removeAllShapes();
    	drawingFrame.getDrawingView().repaint();
    	drawingFrame.getLogView().getModel().clear();
	}
	

	public Color getOutlineColor() {
		return outlineColor;
	}

	public Color getInteriorColor() {
		return interiorColor;
	}

}
