package mvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

import mvc.controller.ButtonController;
import shape.Command;
import shape.Shape;
import shape.observer.ButtonObserver;
import shape.observer.Observer;
import shape.observer.Subject;

public class DrawingModel implements Subject, Serializable{
	
	static final long serialVersionUID = 1L;

	private ArrayList<Shape> shapes = new ArrayList<>();
	private Deque<Command> redoStack = new LinkedList<>();
	private Deque<Command> undoStack = new LinkedList<>();
	private ArrayList<Shape> selectedShapes = new ArrayList<>();
	private ArrayList<String> logList  = new ArrayList<>();
	
	ArrayList<Observer> observers=new ArrayList<>();
	
	

	public void addShape(Shape shape) {
		shapes.add(shape);
	
	}

	public void removeShape(Shape shape) {
		shapes.remove(shape);
	}
	
	public void removeAllShapes() {
		shapes.clear();
		redoStack.clear();
		undoStack.clear();
		
	}

	public Shape getShape(int i) {
		return shapes.get(i);
	}

	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	public void setShapes(ArrayList<Shape> shapes) {
		this.shapes = shapes;
	}

	public Deque<Command> getRedoStack() {
		return redoStack;
	}

	public Deque<Command> getUndoStack() {
		return undoStack;
	}

	public ArrayList<Shape> getSelectedShapes() {
		return selectedShapes;
	}

	public void setSelectedShapes(ArrayList<Shape> selectedShapes) {
		this.selectedShapes = selectedShapes;
	}

	@Override
	public void addObserver(Observer observer) {
		System.out.println("Add observer!");
		observers.add(observer);
		
	}

	@Override
	public void deleteObserver(Observer observer) {
		
		observers.remove(observer);
		
	}

	@Override
	public void notifyAllObservers() {
		
		System.out.println("U notify" + selectedShapes.size());
		for (Observer observer : observers) {
			countSelectedShapes();
			observer.update(selectedShapes.size());
			System.out.println("Observer: " + observer);
		
		}
	
	}
	
	public int countSelectedShapes() {
		selectedShapes.clear();
		for (int i = shapes.size() - 1; i >= 0; i--) {
			if (shapes.get(i).isSelected()) {
				selectedShapes.add(shapes.get(i));
			}
		}
		
		return selectedShapes.size();
	}

	public ArrayList<String> getLogList() {
		return logList;
	}

	public void setLogList(ArrayList<String> logList) {
		this.logList = logList;
	}
	
	
}
