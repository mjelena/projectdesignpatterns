package app;

import frame.DrawingFrame;
import mvc.controller.ButtonController;
import mvc.controller.DrawingController;
import mvc.model.DrawingModel;
import shape.observer.ButtonObserver;

public class DrawingApp {

	public static void main(String[] args) {

		DrawingFrame drawingFrame = new DrawingFrame();
		DrawingModel drawingModel = new DrawingModel();
		
		DrawingController drawingController = new DrawingController(drawingModel, drawingFrame);
		drawingFrame.setDrawingController(drawingController);
		drawingFrame.getDrawingView().setDrawingmodel(drawingModel);

		ButtonController buttonController = new ButtonController(drawingFrame, drawingModel);
		drawingFrame.setButtonController(buttonController);
		
	
		
		ButtonObserver buttonObserver = new ButtonObserver(drawingFrame.getButtonView(), drawingFrame.getButtonViewRight());
		drawingModel.addObserver(buttonObserver);
		
	
		
		
		
		
		
	}

}
