package zcommand;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;
import shape.Shape;

public class BringToBack implements Command {
	
	private DrawingModel drawingModel;
	private LogView logView;
	private Shape tmpShape;
	private int indexOfShape;

	public BringToBack(DrawingModel drawingModel, LogView logView) {
		this.drawingModel = drawingModel;
		this.logView = logView;
	}

	@Override
	public void execute() {
		
		
		for(int i = 0 ; i < drawingModel.getShapes().size() ; i++) {
			if(drawingModel.getShapes().get(i) == drawingModel.getSelectedShapes().get(0) ) {
				tmpShape = drawingModel.getSelectedShapes().get(0);
				indexOfShape = i;
				for(int j=i ; j >=1 ; j--) {
					
					drawingModel.getShapes().set(j, drawingModel.getShape(j-1));
				}
				drawingModel.getShapes().set(0, tmpShape);
				logView.getModel().addElement("Bring to back: " + tmpShape.toString());
				return;
			}
		}
	}

	@Override
	public void unexecute() {
	
		for(int i=0; i < indexOfShape ; i++) {
			drawingModel.getShapes().set(i, drawingModel.getShape(i+1));
			
		}
		
		drawingModel.getShapes().set(indexOfShape, tmpShape);
		logView.getModel().addElement("Undo bring to back: " + tmpShape.toString());

	}

}
