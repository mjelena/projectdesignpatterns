package zcommand;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;
import shape.Shape;

public class BringToFront implements Command {
	
	private DrawingModel drawingModel;
	private LogView logView;
	private Shape tmpShape;
	private int indexOfShape;

	public BringToFront(DrawingModel drawingModel, LogView logView) {
		this.drawingModel = drawingModel;
		this.logView = logView;
	}

	@Override
	public void execute() {
		
		for(int i = 0 ; i < drawingModel.getShapes().size() ; i++) {
			if(drawingModel.getShapes().get(i) == drawingModel.getSelectedShapes().get(0) ) {
				tmpShape = drawingModel.getSelectedShapes().get(0);
				indexOfShape = i;
				
				drawingModel.getShapes().add(tmpShape);
				drawingModel.removeShape(drawingModel.getShape(i));
				logView.getModel().addElement("Bring to front: " + tmpShape.toString());
				return;
			}
		}
		

	}

	@Override
	public void unexecute() {
	
		
		
		for(int i= drawingModel.getShapes().size() - 1; i > indexOfShape ; i--) {
			drawingModel.getShapes().set(i, drawingModel.getShape(i-1));
			
		}
		
		drawingModel.getShapes().set(indexOfShape, tmpShape);
		logView.getModel().addElement("Undo bring to front: " + tmpShape.toString());
		
		
		

	}

}
