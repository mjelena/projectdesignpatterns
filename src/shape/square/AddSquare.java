package shape.square;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class AddSquare implements Command {

	private DrawingModel drawingModel;
	private Square square;
	private LogView logView;

	public AddSquare(DrawingModel model, Square square, LogView logView) {
		this.drawingModel = model;
		this.square = square;
		this.logView = logView;
	}

	@Override
	public void execute() {
		drawingModel.addShape(square);
		logView.getModel().addElement("Add: " + square.toString());

	}

	@Override
	public void unexecute() {
		drawingModel.removeShape(square);
		logView.getModel().addElement("Undo add: " + square.toString());

	}
}

