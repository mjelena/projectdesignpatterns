package shape.square;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class RemoveSquare implements Command {

	private DrawingModel model;
	private Square square;
	private LogView logView;

	public RemoveSquare(DrawingModel model, Square square, LogView logView) {
		this.model = model;
		this.square = square;
		this.logView = logView;
	}

	@Override
	public void execute() {
		model.removeShape(square);
		logView.getModel().addElement("Delete: " + square.toString());
		

	}

	@Override
	public void unexecute() {
		model.addShape(square);
		logView.getModel().addElement("Undo delete: " + square.toString());
		

	}
}

