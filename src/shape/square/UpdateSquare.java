package shape.square;

import mvc.view.LogView;
import shape.Command;
import shape.point.Point;

public class UpdateSquare implements Command {
	
	private Square originalSquare; 
	private Square newSquare; 
	private Square tmpSquare; 
	private LogView logView;
	
	public UpdateSquare(Square originalSquare, Square newSquare, LogView logView) {
		this.originalSquare=originalSquare;
		this.newSquare=newSquare;
		this.logView=logView;
	}

	@Override
	public void execute() {
		tmpSquare=new Square(new Point(originalSquare.getPointUpLeft().getX(), originalSquare.getPointUpLeft().getY(), originalSquare.getPointUpLeft().getColor()), originalSquare.getLengthSide(), originalSquare.getColor(), originalSquare.getInsideColor());
		originalSquare.getPointUpLeft().setX(newSquare.getPointUpLeft().getX());
		originalSquare.getPointUpLeft().setY(newSquare.getPointUpLeft().getY());
		originalSquare.getPointUpLeft().setColor(newSquare.getPointUpLeft().getColor());
		originalSquare.setLengthSide(newSquare.getLengthSide());
		originalSquare.setColor(newSquare.getColor());
		originalSquare.setInsideColor(newSquare.getInsideColor());
		logView.getModel().addElement("Update: " + originalSquare.toString());
	}

	@Override
	public void unexecute() {
		originalSquare.getPointUpLeft().setX(tmpSquare.getPointUpLeft().getX());
		originalSquare.getPointUpLeft().setY(tmpSquare.getPointUpLeft().getY());
		originalSquare.getPointUpLeft().setColor(tmpSquare.getPointUpLeft().getColor());
		originalSquare.setLengthSide(tmpSquare.getLengthSide());
		originalSquare.setColor(tmpSquare.getColor());
		originalSquare.setInsideColor(tmpSquare.getInsideColor());
		logView.getModel().addElement("Undo upadate: " + originalSquare.toString());
		
	}

}



