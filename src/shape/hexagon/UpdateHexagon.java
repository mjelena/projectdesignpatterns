package shape.hexagon;

import mvc.view.LogView;
import shape.Command;

public class UpdateHexagon implements Command {
	
	private HexagonAdapter originalHexagon; 
	private HexagonAdapter newHexagon; 
	private HexagonAdapter tmpHexagon; 
	private LogView logView;
	
	public UpdateHexagon(HexagonAdapter originalHexagon, HexagonAdapter newhexagon, LogView logView) {
		this.originalHexagon=originalHexagon;
		this.newHexagon=newhexagon;
		this.logView = logView;
	}

	@Override
	public void execute() {
		System.out.println(originalHexagon);
		tmpHexagon=new HexagonAdapter(originalHexagon.getHexagon(), originalHexagon.getHexagon().getBorderColor(), originalHexagon.getHexagon().getAreaColor());
		originalHexagon.getHexagon().setX(newHexagon.getHexagon().getX());
		originalHexagon.getHexagon().setY(newHexagon.getHexagon().getY());
		originalHexagon.getHexagon().setR(newHexagon.getHexagon().getR());
		originalHexagon.getHexagon().setBorderColor(newHexagon.getHexagon().getBorderColor());
		originalHexagon.getHexagon().setAreaColor(newHexagon.getHexagon().getAreaColor());
		logView.getModel().addElement("Update: " + originalHexagon.toString());
		System.out.println(tmpHexagon);
		System.out.println(originalHexagon);
	}

	@Override
	public void unexecute() {
		originalHexagon.getHexagon().setX(tmpHexagon.getHexagon().getX());
		originalHexagon.getHexagon().setY(tmpHexagon.getHexagon().getY());
		originalHexagon.getHexagon().setR(tmpHexagon.getHexagon().getR());
		originalHexagon.getHexagon().setBorderColor(tmpHexagon.getHexagon().getBorderColor());
		originalHexagon.getHexagon().setAreaColor(tmpHexagon.getHexagon().getAreaColor());
		logView.getModel().addElement("Undo update: " + originalHexagon.toString());
		System.out.println(tmpHexagon);
		System.out.println(originalHexagon);
		
		
	}

}



