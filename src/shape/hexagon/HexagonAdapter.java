package shape.hexagon;

import java.awt.Color;
import java.awt.Graphics;

import hexagon.Hexagon;
import shape.Moveable;
import shape.SurfaceShape;
import shape.square.Square;

public class HexagonAdapter extends SurfaceShape implements Moveable{
	
	private Hexagon hexagon;
	public HexagonAdapter(Hexagon hexagon) {
		this.hexagon=hexagon;
	}
	
	public HexagonAdapter(Hexagon hexagon, Color color) {
		this(hexagon);
		hexagon.setBorderColor(color);
	}
	
	public HexagonAdapter(Hexagon hexagon, Color color, Color insideColor) {
		this(hexagon, color);
		hexagon.setAreaColor(insideColor);
	}
	
	public String toString() {
		return "Hexagon: (" + this.hexagon.getX() + "," + this.hexagon.getY() + "); radius:" + hexagon.getR() + "; outer color:" + hexagon.getBorderColor().getRGB()+ "; inside color:" + hexagon.getAreaColor().getRGB();
	}

	public boolean equals(Object obj) {
		if (obj instanceof HexagonAdapter) {
			HexagonAdapter temp = ((HexagonAdapter) obj);
			if(hexagon.getX() == temp.getHexagon().getX() && hexagon.getY()==temp.getHexagon().getY() && hexagon.getR() == temp.getHexagon().getR())
			return true;
			else
				return false;
		} else
			return false;
	}

	public double area() {
		return 3*Math.sqrt(3)*hexagon.getR()/2;
	}
	
	
	@Override
	public int compareTo(Object o) {
		if(o instanceof HexagonAdapter){
			HexagonAdapter temp  = (HexagonAdapter) o;
			return hexagon.getR()-temp.getHexagon().getR();
		}
		else 
			return 0;
	}

	@Override
	public void moveTo(int x, int y) {
		hexagon.setX(x);
		hexagon.setY(y);
	}

	@Override
	public void moveFor(int x, int y) {
		hexagon.setX(hexagon.getX() + x);
		hexagon.setY(hexagon.getY() + y);
		
	}

	@Override
	public void fill(Graphics g) {
		g.setColor(getInsideColor());
		g.fillRect(hexagon.getX()+1, hexagon.getY()+1, hexagon.getR()-1, hexagon.getR()-1);
		
	}

	@Override
	public void drawShape(Graphics g) {
		hexagon.paint(g);
	}

	@Override
	public void selected(Graphics g) {
		g.setColor(Color.BLUE);
	
		
	}

	@Override
	public boolean contains(int x, int y) {
		return hexagon.doesContain(x, y);
	}

	public Hexagon getHexagon() {
		return hexagon;
	}

	public boolean isSelected() {
		return hexagon.isSelected();
	}
	public void setSelected(boolean selected) {
		hexagon.setSelected(selected); 
	}
}
