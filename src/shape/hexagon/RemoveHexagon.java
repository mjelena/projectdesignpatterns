package shape.hexagon;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class RemoveHexagon implements Command {

	private DrawingModel model;
	private HexagonAdapter hexagon;
	private LogView logView;

	public RemoveHexagon(DrawingModel model, HexagonAdapter hexagon, LogView logView) {
		this.model = model;
		this.hexagon = hexagon;
		this.logView = logView;
	}

	@Override
	public void execute() {
		model.removeShape(hexagon);
		logView.getModel().addElement("Delete: " + hexagon.toString());
		

	}

	@Override
	public void unexecute() {
		model.addShape(hexagon);
		logView.getModel().addElement("Undo delete: " + hexagon.toString());

	}
}

