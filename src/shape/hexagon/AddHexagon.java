package shape.hexagon;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class AddHexagon implements Command {

	private DrawingModel drawingModel;
	private HexagonAdapter hexagon;
	private LogView logView;

	public AddHexagon(DrawingModel model, HexagonAdapter hexagon, LogView logView) {
		this.drawingModel = model;
		this.hexagon = hexagon;
		this.logView = logView;
	}

	@Override
	public void execute() {
		drawingModel.addShape(hexagon);
		logView.getModel().addElement("Add: " + hexagon.toString());

	}

	@Override
	public void unexecute() {
		drawingModel.removeShape(hexagon);
		logView.getModel().addElement("Undo add: " + hexagon.toString());

	}
}

