package shape.line;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class RemoveLine implements Command {

	private DrawingModel model;
	private Line line;
	private LogView logView;
	
	public RemoveLine(DrawingModel model, Line line, LogView logView) {
		this.model=model;
		this.line=line;
		this.logView=logView;
	}
	
	@Override
	public void execute() {
		model.removeShape(line);	
		logView.getModel().addElement("Delete: " + line.toString());
	}

	@Override
	public void unexecute() {
		model.addShape(line);
		logView.getModel().addElement("Undo delete: " + line.toString());
	}
}

