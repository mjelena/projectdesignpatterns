package shape.line;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class AddLine implements Command {

	private DrawingModel drawingModel;
	private Line line;
	private LogView logView;

	public AddLine(DrawingModel drawingModel, Line line, LogView logView) {
		this.drawingModel = drawingModel;
		this.line = line;
		this.logView = logView;
	}

	@Override
	public void execute() {
		drawingModel.addShape(line);
		logView.getModel().addElement("Add: " + line.toString());
		

	}

	@Override
	public void unexecute() {
		drawingModel.removeShape(line);
		logView.getModel().addElement("Undo add: " + line.toString());

	}
}

