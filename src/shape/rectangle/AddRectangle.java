package shape.rectangle;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class AddRectangle implements Command {

	private DrawingModel drawingModel;
	private Rectangle rectangle;
	private LogView logView;

	public AddRectangle(DrawingModel drawingModel, Rectangle rectangle, LogView logView) {
		this.drawingModel = drawingModel;
		this.rectangle = rectangle;
		this.logView = logView;
	}

	@Override
	public void execute() {
		drawingModel.addShape(rectangle);
		logView.getModel().addElement("Add: " + rectangle.toString());

	}

	@Override
	public void unexecute() {
		drawingModel.removeShape(rectangle);
		logView.getModel().addElement("Undo add: " + rectangle.toString());

	}
}


