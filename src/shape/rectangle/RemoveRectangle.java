package shape.rectangle;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class RemoveRectangle implements Command {

	private DrawingModel model;
	private Rectangle rectangle;
	private LogView logView;

	public RemoveRectangle(DrawingModel model, Rectangle rectangle, LogView logView) {
		this.model = model;
		this.rectangle = rectangle;
		this.logView = logView;
	}

	@Override
	public void execute() {
		model.removeShape(rectangle);
		logView.getModel().addElement("Delete: " + rectangle.toString());

	}

	@Override
	public void unexecute() {
		model.addShape(rectangle);
		logView.getModel().addElement("Undo delete: " + rectangle.toString());

	}
}



