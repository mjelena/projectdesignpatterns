package shape.point;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class AddPoint implements Command {

	private DrawingModel drawingModel;
	private Point point;
	private LogView logView;

	public AddPoint(DrawingModel drawingModel, Point point, LogView logView) {
		this.drawingModel = drawingModel;
		this.point = point;
		this.logView = logView;
	}

	@Override
	public void execute() {
		drawingModel.addShape(point);
		logView.getModel().addElement("Add: " + point.toString());

	}

	@Override
	public void unexecute() {
		drawingModel.removeShape(point);
		logView.getModel().addElement("Undo add: " + point.toString());

	}
}
