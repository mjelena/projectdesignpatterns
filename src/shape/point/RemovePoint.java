package shape.point;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class RemovePoint implements Command {

	private DrawingModel model;
	private Point point;
	private LogView logView;
	
	public RemovePoint(DrawingModel model, Point point, LogView logView) {
		this.model=model;
		this.point=point;
		this.logView=logView;
		
	}
	
	@Override
	public void execute() {
		model.removeShape(point);	
		logView.getModel().addElement("Delete: " + point.toString());
	}

	@Override
	public void unexecute() {
		model.addShape(point);
		logView.getModel().addElement("Undo delete: " + point.toString());
	}

}
