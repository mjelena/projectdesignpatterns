package shape.observer;

import mvc.view.ButtonView;
import mvc.view.ButtonViewRight;

public class ButtonObserver implements Observer {
	
	private ButtonView buttonView;
	private ButtonViewRight buttonViewRight;

	public ButtonObserver(ButtonView buttonView, ButtonViewRight buttonViewRight) {
		this.buttonView = buttonView;
		this.buttonViewRight = buttonViewRight;
	}
	
	@Override
	public void update(int numberOfSelectedShapes) {
		System.out.println("Number of selected in button observer: " + numberOfSelectedShapes);
		
		if(numberOfSelectedShapes == 1) {
			buttonView.getBtnUpdate().setEnabled(true);
			buttonView.getBtnDelete().setEnabled(true);
			buttonViewRight.getBtnBringToBack().setEnabled(true);
			buttonViewRight.getBtnBringToFront().setEnabled(true);
			buttonViewRight.getBtnToBack().setEnabled(true);
			buttonViewRight.getBtnToFront().setEnabled(true);
			
		} 
		else if (numberOfSelectedShapes > 1) {
			buttonView.getBtnUpdate().setEnabled(false);
			
			buttonViewRight.getBtnBringToBack().setEnabled(false);
			buttonViewRight.getBtnBringToFront().setEnabled(false);
			buttonViewRight.getBtnToBack().setEnabled(false);
			buttonViewRight.getBtnToFront().setEnabled(false);
		}
		if(numberOfSelectedShapes == 0) {
			buttonView.getBtnUpdate().setEnabled(false);
			buttonView.getBtnDelete().setEnabled(false);
			
			buttonViewRight.getBtnBringToBack().setEnabled(false);
			buttonViewRight.getBtnBringToFront().setEnabled(false);
			buttonViewRight.getBtnToBack().setEnabled(false);
			buttonViewRight.getBtnToFront().setEnabled(false);
		}
		
	}

}
