package shape.observer;

public interface Observer {

	public void update(int numberOfSelectedShapes);
}
