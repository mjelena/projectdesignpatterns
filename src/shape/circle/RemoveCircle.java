package shape.circle;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class RemoveCircle implements Command {

	private DrawingModel model;
	private Circle circle;
	private LogView logView;

	public RemoveCircle(DrawingModel model, Circle circle, LogView logView) {
		this.model = model;
		this.circle = circle;
		this.logView = logView;
	}

	@Override
	public void execute() {
		model.removeShape(circle);
		logView.getModel().addElement("Delete: " + circle.toString());

	}

	@Override
	public void unexecute() {
		model.addShape(circle);
		logView.getModel().addElement("Undo delete: " + circle.toString());
		

	}
}


