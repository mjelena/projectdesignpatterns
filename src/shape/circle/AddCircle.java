package shape.circle;

import mvc.model.DrawingModel;
import mvc.view.LogView;
import shape.Command;

public class AddCircle implements Command {

	private DrawingModel drawingModel;
	private Circle circle;
	private LogView logView;

	public AddCircle(DrawingModel drawingModel, Circle circle, LogView logView) {
		this.drawingModel = drawingModel;
		this.circle = circle;
		this.logView = logView;
	}

	@Override
	public void execute() {
		drawingModel.addShape(circle);
		logView.getModel().addElement("Add: " + circle.toString());

	}

	@Override
	public void unexecute() {
		drawingModel.removeShape(circle);
		logView.getModel().addElement("Undo add: " + circle.toString());

	}
}

