package frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;

import mvc.controller.ButtonController;
import mvc.controller.DrawingController;
import mvc.view.ButtonView;
import mvc.view.ButtonViewLeft;
import mvc.view.ButtonViewRight;
import mvc.view.DrawingView;
import mvc.view.LogView;


public class DrawingFrame extends JFrame {
	private DrawingView drawingView=new DrawingView();
	private DrawingController drawingController;
	private ButtonView buttonView=new ButtonView();
	private ButtonController buttonController;
	private LogView logView = new LogView();
	private ButtonViewLeft buttonViewLeft = new ButtonViewLeft();
	private ButtonViewRight buttonViewRight = new ButtonViewRight();

	
	public DrawingFrame() {
		setSize(640,600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setTitle("Jelena Medarevic IT50-2015");
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().add(drawingView, BorderLayout.CENTER);
		getContentPane().add(buttonView, BorderLayout.NORTH);
		getContentPane().add(logView, BorderLayout.SOUTH);
		getContentPane().add(buttonViewLeft, BorderLayout.WEST);
		getContentPane().add(buttonViewRight, BorderLayout.EAST);
		
		drawingView.setBackground(Color.WHITE);
		
		
		drawingView.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(!buttonView.getTglbtnSelected().isSelected()) {
					buttonView.getComboBox_Shapes().setEnabled(true);
					drawingController.handleMousePressed(e);
				} else {
					buttonView.getComboBox_Shapes().setEnabled(false);
					
					buttonController.selectedClick(e.getX(),e.getY());
					
				}
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if(!buttonView.getTglbtnSelected().isSelected()) {
					drawingController.drawShape(e);
				}	
			}
		});
		
		drawingView.addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				if(!buttonView.getTglbtnSelected().isSelected()) {
				drawingController.handleMouseDragged(e);
			}
			}
		
		});
		
	
		
		buttonView.getBtnUndo().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				buttonController.undoClick();
			}
		});
		
		buttonView.getBtnRedo().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				buttonController.redoClick();
				
			}
		});
		
	
		
		buttonView.getTglbtnSelected().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!buttonView.getTglbtnSelected().isSelected()) {
					buttonController.unselectArray();
					buttonView.getComboBox_Shapes().setEnabled(true);
				} else
				buttonView.getComboBox_Shapes().setEnabled(false);
				
			}
		});
		
		
		buttonView.getBtnDelete().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonController.deleteShape();
				
			}
		});
			
		
		buttonView.getBtnUpdate().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonController.updateShape();
				
			}
		});
		
		buttonView.getBtnOutlineColor().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.changeOutlineColor();
			}
		});
		
		buttonView.getBtnInteriorColor().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.changeInteriorColor();
			}
		});
		
		buttonViewLeft.getBtnSaveLog().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				buttonController.saveLog();
			}
		});
		
		buttonViewLeft.getBtnSaveDrawing().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.saveDrawing();
			}
		});
		
		buttonViewLeft.getBtnImportLog().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				buttonController.importLog();
			}
		});
		
		buttonViewLeft.getBtnLogNext().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				buttonController.logNextLine();
			}
		});
		
		buttonViewLeft.getBtnImportAllLog().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.importAllLog();
			}
		});
		
		buttonViewRight.getBtnToFront().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
			}
		});
		
		buttonViewRight.getBtnToBack().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
			}
		});
		
		buttonViewRight.getBtnBringToFront().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.bringToFront();
			}
		});
		
		buttonViewRight.getBtnBringToBack().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.bringToBack();
			}
		});
		
		buttonViewRight.getBtnToFront().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.toFront();
			}
		});
		
		buttonViewRight.getBtnToBack().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				buttonController.toBack();
			}
		});
		
		buttonViewLeft.getBtnImportDrawing().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				buttonController.importDrawing();
			}
		});
		
		buttonViewLeft.getBtnNewDrawing().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				buttonController.newDrawing();
			}
		});
		
		
	}
	
	public DrawingView getDrawingView() {
		return drawingView;
	}
	public void setDrawingView(DrawingView drawingView) {
		this.drawingView = drawingView;
	}

	public DrawingController getDrawingController() {
		return drawingController;
	}

	public void setDrawingController(DrawingController drawingController) {
		this.drawingController = drawingController;
	}
	
	public ButtonController getButtonController() {
		return buttonController;
	}

	public void setButtonController(ButtonController buttonController) {
		this.buttonController = buttonController;
	}

	public ButtonView getButtonView() {
		return buttonView;
	}

	public LogView getLogView() {
		return logView;
	}

	public void setLogView(LogView logView) {
		this.logView = logView;
		
		
	}

	public ButtonViewRight getButtonViewRight() {
		return buttonViewRight;
	}

	public ButtonViewLeft getButtonViewLeft() {
		return buttonViewLeft;
	}


	
	
}
